import { OnInit, ViewChild, ElementRef, Component } from '@angular/core';
import { NavbarInterceptor } from 'src/app/dashboard/services/navbar-interceptor.service';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

    constructor(
      private navbarIntercepor: NavbarInterceptor
    ) {}

    @ViewChild('navBurger', {static: true}) private navBurger: ElementRef;
    @ViewChild('navMenu', {static: true}) private navMenu: ElementRef;

    ngOnInit(): void {
      this.navbarIntercepor.toggleFunc = this.toggleNavbar.bind(this);
    }

    public toggleNavbar() {
        this.navBurger.nativeElement.classList.toggle('is-active');
        this.navMenu.nativeElement.classList.toggle('is-active');
    }

    public toggleAsideMenu() {

    }
}
