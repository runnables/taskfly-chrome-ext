import { NgModule } from '@angular/core';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    MatIconModule,
    HttpClientModule
  ],
  exports: [
    MatIconModule
  ]
})
export class MaterialModule {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    this.matIconRegistry.addSvgIcon(
      'github',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/github-logo.svg')
    );
    this.matIconRegistry.addSvgIcon(
      'gitlab',
      this.domSanitizer.bypassSecurityTrustResourceUrl('assets/svg/gitlab-icon-rgb.svg')
    );
  }
}
