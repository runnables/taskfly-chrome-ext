import { Injectable, ViewChild } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { NavbarComponent } from 'src/app/core/components/navbar/navbar.component';

@Injectable()
export class NavbarInterceptor implements CanActivateChild {
  public toggleFunc: () => void;

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | import("@angular/router").UrlTree | Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {
    if (this.toggleFunc != null) { this.toggleFunc(); }
    return true;
  }
}
