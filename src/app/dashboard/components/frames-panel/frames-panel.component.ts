import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-frames-panel',
  templateUrl: './frames-panel.component.html',
  styleUrls: ['./frames-panel.component.sass']
})

export class FramesPanelComponent implements OnInit {
  @Input() tab: chrome.tabs.Tab;

  constructor() { }

  ngOnInit() { }
}
