import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-tasks-panel',
  templateUrl: './tasks-panel.component.html',
  styleUrls: ['./tasks-panel.component.sass'],
})

export class TasksPanelComponent implements OnInit {
  @Input() tab: chrome.tabs.Tab;

  // Page elements
  private menuElement: HTMLDivElement;
  private taskbarElement: HTMLDivElement;
  private filterbarElement: HTMLDivElement;
  private quadraoElement: HTMLDivElement;

  constructor() { }

  ngOnInit() {

  }

  cbpQuerySelector(selector: string, all = false) {
    // if (all) {
    //   return this.cbp.document.querySelectorAll(selector);
    // } else {
    //   return this.cbp.document.querySelector(selector);
    // }
  }

  initElementsAndStates() {
    // this.menuElement = this.cbpQuerySelector('body > div:nth-child(2) > div.menu') as HTMLDivElement;
    // this.taskbarElement = this.cbpQuerySelector('body > div:nth-child(2) > div.content-wrapper > section.content.container') as HTMLDivElement;
    // this.filterbarElement = this.cbpQuerySelector('#container-layout > div.barra-ferramentas.container') as HTMLDivElement;
    // this.quadraoElement = this.cbpQuerySelector('#container-layout > div.quadrao') as HTMLDivElement;
  }

  fullscreenToggle(target) {
    if (target.checked) {
      this.taskbarElement.style.display = this.filterbarElement.style.display = this.menuElement.style.display =  'none';
      this.quadraoElement.style.height = '100vh';
      this.quadraoElement.querySelectorAll('div ul').forEach((quadro: HTMLDivElement) => quadro.style.maxHeight = '');
    } else {

    }
  }
}
