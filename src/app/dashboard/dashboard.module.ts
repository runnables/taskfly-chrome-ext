import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './pages/dashboard.component';
import { CreditsComponent } from './components/credits/credits.component';

const ROUTES: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'credits', component: CreditsComponent}
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        RouterModule.forChild(ROUTES)
    ],
    exports: [
    ]
})
export class DashboardModule {}
