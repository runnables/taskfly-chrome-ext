import { Component, ViewChild, ViewContainerRef, ComponentRef, ComponentFactoryResolver, ComponentFactory, OnInit, OnDestroy } from '@angular/core';
import { TasksPanelComponent } from '../components/tasks-panel/tasks-panel.component';
import { FramesPanelComponent } from '../components/frames-panel/frames-panel.component';

type PanelComponent = TasksPanelComponent | FramesPanelComponent;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.sass']
})
export class DashboardComponent implements OnInit, OnDestroy {
    @ViewChild('panel_options', {read: ViewContainerRef}) container: ViewContainerRef;

    componentRef: ComponentRef<PanelComponent>;

    constructor(
      private resolver: ComponentFactoryResolver
    ) {}

    ngOnInit(): void {
      chrome.tabs.query({ active: true, currentWindow: true }, ([{ ...tab }]) => {
        const url = tab.url.split('/')[3];
        if (url === 'Tarefa') {
          this.createComponent(TasksPanelComponent, tab);
        } else if (url === 'Quadros') {
          this.createComponent(FramesPanelComponent, tab);
        }
      });
    }

    ngOnDestroy(): void {
      this.componentRef.destroy();
    }

    createComponent(component: any, tab: chrome.tabs.Tab) {
      this.container.clear();
      const factory: ComponentFactory<TasksPanelComponent> = this.resolver.resolveComponentFactory(component);
      this.componentRef = this.container.createComponent(factory);
      // If has a input
      this.componentRef.instance.tab = tab;
      // If has a output
      // this.componentRef.instance.[output].subscript(e => e);
    }
}
